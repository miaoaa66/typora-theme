# typora-theme

#### 介绍
typora主题仓库，推荐一些简洁好看的typora主题。    
typora：一个md文件编辑器。   
typora官网：https://typoraio.cn/。   
typora官网主题地址：https://theme.typoraio.cn/。   

有用的话就star支持一下



#### 适用typora版本

version 0.9.72(beta)



#### 软件架构

推荐如下(不分先后)

##### **1：See Yue 望月**

​	1.1：官方仓库：[jinghu-moon/typora-see-yue-theme: See Yue 主题是一个自定义样式极多、简约、充满细节的 Typora 明亮主题。（See Yue theme is a bright typora theme with many custom styles, simplicity and full of details.） (github.com)](https://github.com/jinghu-moon/typora-see-yue-theme)

​	1.2：图例：https://theme.typoraio.cn/media/theme/see-yue/1.png

##### **2：dracula**

​	2.1：官方仓库：[Teyler7/dracula-typora-theme: A dark theme for Typora inspired by the Dracula color scheme and Material Design. (github.com)](https://github.com/Teyler7/dracula-typora-theme)

​	2.2：图例：https://theme.typoraio.cn/media/theme/dracula/screenshot2.png

##### **3：github**

​	3.1：官方仓库：[typora/typora-default-themes: default themes used in Typora (github.com)](https://github.com/typora/typora-default-themes)

​	3.2：图例：https://theme.typoraio.cn/media/theme/github/Snip20170320_1.png

##### **持续更新**




#### 主题更换教程

1. 下载typora，官方地址[Typora 官方中文站 (typoraio.cn)](https://typoraio.cn/)

2. 在本仓库或其他渠道下载对应样式文件

3. 以下演示使用本仓库下载并更换主题（以《See Yue 望月》为例）

   3.1.克隆地址https://gitee.com/miaoaa66/typora-theme.git

   3.2.进入项目根目录（cd typora-theme）

   3.3.进入对应主题文件夹，如望月（cd typora-see-yue-theme）

   3.4.复制对应的主题文件，如望月（typora-see-yue-theme文件夹下的SeeYue文件夹和see-yue.css）

   3.5.使用typora打开任意一个md文件，点击左上角文件->偏好设置->打开主题文件夹->将3.4复制的文件粘贴到此文件夹-> 使用typora重新打开任意md文件->点击上方主题->选择对应的主题名字即可(如望月See You)

   3.6.这样就完成了

   

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
